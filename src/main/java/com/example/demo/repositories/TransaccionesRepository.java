package com.example.demo.repositories;
import java.util.List;

import com.example.demo.model.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface TransaccionesRepository extends JpaRepository<Transacciones, Long>{
    @Query(
        value = "SELECT * FROM transacciones i WHERE i.user_id = ?",
        nativeQuery = true
    )
    List<Transacciones> findByUserId(long userId);
}
