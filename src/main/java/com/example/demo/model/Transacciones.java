package com.example.demo.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "transacciones")
public class Transacciones {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "tipoTra")
    private String tipoTra;

    @Column(name = "benefTra")
    private String benefTra;

    @Column(name = "fechaTra")
    private String fechaTra;

    @Column(name = "numTra")
    private Integer numTra;

    @Column(name = "user_id")
    private long userId;

    public Transacciones(String tipoTra, String benefTra, String fechaTra, Integer numTra, long userId) {
        this.tipoTra = tipoTra;
        this.benefTra = benefTra;
        this.fechaTra = fechaTra;
        this.numTra = numTra;
        this.userId = userId;
    }





   



}
