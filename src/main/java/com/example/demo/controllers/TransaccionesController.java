package com.example.demo.controllers;

import java.util.List;

import com.example.demo.model.*;
import com.example.demo.repositories.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/transacciones")
public class TransaccionesController {

    @Autowired
    TransaccionesRepository TransaccionesRepository;

    @GetMapping("/tra")
    public ResponseEntity<List<Transacciones>> findReportsByUserId(@RequestParam(required = true) long id) {
        List<Transacciones> transacciones = TransaccionesRepository.findByUserId(id);
        return new ResponseEntity<>(transacciones, HttpStatus.OK);
    }

    @PostMapping("/tra")
    public ResponseEntity<Transacciones> registerReports(@RequestBody Transacciones Transacciones) {
        try {
            Transacciones created = TransaccionesRepository.save(
                    new Transacciones(Transacciones.getTipoTra(),Transacciones.getBenefTra(),Transacciones.getFechaTra(),Transacciones.getNumTra(), Transacciones.getUserId()));
            return new ResponseEntity<>(created, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
